# Majel: Scaffolding

A wrapper around [Majel](https://gitlab.com/danielquinn/majel) and [Mycroft](https://mycroft.ai/)
([Docker](https://hub.docker.com/r/mycroftai/docker-mycroft)) to get everything
up and running on the same machine.

Theoretically, Majel will work just fine with Mycroft running on one device and
Majel listening to the message bus from an entirely different machine, but this
is a handy way to get everything up and running from one device.

This works really nicely on a tablet, like the Surface Pro 3. I have this setup
in my kitchen :-)


## The State of Things

Mycroft is an amazing piece of software, but it's not very easy to install.
You can't just say `apt install mycroft`, so the next-best-option for people
wanting to isolate an experiment from the rest of their system is [Docker](https://www.docker.com/).
Unfortunately, the docker installation isn't very common in Mycroftland, so
support for it is spotty.  Some of the weird behaviour you might run into while
using it is documented here along with work-arounds so hopefully you won't have
any problems.

Ideally, I'd like to be able to just run Majel on the desktop while Mycroft is
run on a dedicated device somewhere else on the network, but for the moment
anyway, there's some quirks I need to work out to get that working, not the
least of which would be getting all of the Majel skills into the Mycroft market
and finding a way for the [bookmarks-skill](https://gitlab.com/danielquinn/bookmarks-skill)
to pull Firefox bookmarks over the network somehow.


## Installation

I suppose this could have been done with a handy script, but given that every
system is oh-so-slightly-different from every other system, a few simple
instructions just makes more sense.


### System Dependencies

* [Docker](https://www.docker.com/)
* [Poetry](https://python-poetry.org/)
* [MPV](https://mpv.io/)
* [Firefox](https://www.mozilla.org/en-US/firefox/) (The developers edition is ok too)
* [Pulse Audio](https://www.freedesktop.org/wiki/Software/PulseAudio/)
* [Gecko Driver](https://github.com/mozilla/geckodriver)

You'll need to install these system dependencies on your machine.  *How* you do
that varies by Linux distribution, but it's usually something like:

```bash
$ pacman -S python-poetry mpv docker gnome-shell firefox geckodriver
```

> **Note**: Neither Mycroft nor Majel require PulseAudio to function, but the
> Docker deployment assumes your audio is handled by it.  Additionally, we use tmux
> in the `majel-scaffold` script to monitor the output of all the commands.
> If you're planning on starting the two processes manually though, you don't
> need it.


### Python Dependencies

Once you've got the system stuff handled, you'll need to install all the Python
requirements with Poetry:

```bash
$ poetry install
```

This will create a Python virtualenv inside which will be Majel,
docker-compose, and all the other stuff Majel depends on.


## Majel Configuration

[Majel](https://pypi.org/project/majel/) is a front-end for Mycroft that
handles all the browser stuff.  This front-end needs to be configured by
creating a file called `mycroft.yml` in `/etc/`.  Have a look at the [README](https://gitlab.com/danielquinn/majel)
on that project's page for more info.


## Instructions

Now that everything you need is installed, all you should need to do is run:

```bash
$ ./scripts/majel-scaffold start
```

...and wait (it takes a longish time) while the Mycroft image is downloaded &
started, then goes about the setup process.  During this time, Majel's output
will just be a bunch of errors -- you can safely ignore them as it's just Majel
waiting for Mycroft finish starting up.

Part of the Mycroft initialisation process will include him talking to you and
asking you to set him up by going to [home.mycroft.ai](https://home.mycroft.ai/).
You'll need to do that, and while you're there, [configure the skills](https://account.mycroft.ai/skills).
Details on this are in the next section.


### Configuration

Each of the Mycroft skills installed that make up Majel's feature set require
configuration by way of updating skill settings on [home.mycroft.ai](https://home.mycroft.ai/).

* The bookmarks skill needs the path to your Firefox bookmarks file.  This
  scaffold will mount your `${HOME}` directory inside the container at
  `/opt/shared-home`, so the value for this skill will be something like
  `/opt/shared-home/.mozilla/firefox/abcdefgh.default/weave/bookmarks.sqlite`.
* The streamer skill needs a few things including an API key to a free service
  called "Utelly".  Details for these values can be found on the
  [project page for this skill](https://gitlab.com/danielquinn/streamer-skill).
* The Youtube skill requires an API key from Google.  Like the Utelly key, it's
  free, you just have to sign up to get one.  Details for what you need to do
  can be found on [the youtube skill project page](https://gitlab.com/danielquinn/youtube-skill).

After everything is configured, just say:

> *Hey Mycroft, update configuration*

This may well respond with *"No changes to apply"*, but that's often not the
case (don't worry, this is a known bug on Mycroft's end).  This will get your
local setup working with the settings you just entered on the Mycroft site.

> **Note**: If after shutting down Mycroft and later restarting it you find
> that none of the Majel skills work anymore, telling it to
> `update configuration` will probably fix this problem.  I have no idea why
> this is necessary, but it often is.


### Important Note

There's a weird quirk/bug/whatever with Mycroft's Docker implementation that
means that if you `stop` the container with `docker-compose stop`, it will
never start properly again.  I don't know why and the error messages that come
out as a result are too many to make sense of for someone like me who isn't
familiar with the internals of Mycroft and the message bus.

Thankfully, this scaffolding script has a nice work-around: just tear the
container `down` instead of using `stop`:

```bash
$ poetry run docker-compose down
```

This will delete your container, but your configuration will be preserved,
(thanks to the shared volume in `.shared`), so when everything comes back up,
you should be ready to go.


## Use

Once things are up & running, everything should be voice-activated.  Just say
something like:

> *Hey Mycroft, what time is it?*

or to actually make use of some of the new Majel-based functionality:

> *Hey Mycroft, play Altered Carbon*

or

> *Hey Mycroft, Youtube baby shark*

or

> *Hey Mycroft, search my bookmarks for chicken*
